# Luhn JS Validation Services

JavaScript Functions to validate unique identifier number (UIN) with Lunh Algorithm

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
  - [Installation](#installation)
  - [Running the Project](#running-the-project)
- [Folder Structure](#folder-structure)
- [Scripts](#scripts)
- [Contributing](#contributing)
- [License](#license)

## Introduction

You can choice or use these method to customize your script if it suits your needs. JavaScript Functions to validate unique identifier number (UIN) with Lunh Algorithm

## Features

generate & validation methods:
_generateRandomUniqueIdentifierNumberParts_ Generate 9 rendom numbers
_generateUniqueIdentifierNumber_ Generate 10 unique identifier number with key control in the last
_checkUniqueIdentifierNumber_ Check the unique identifier number value
_isValidLuhn_ Check the unique identifier number algorithm inspired by the official documenation https://en.wikipedia.org/wiki/Luhn_algorithm.

## Prerequisites

Ensure you have the following installed:

- [Node.js](https://nodejs.org/) (version v16.20.2 or later)
- [Yarn](https://yarnpkg.com/) (optional but recommended)

## Getting Started

### Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/kotonirina/luhn-js-validation.git
   ```

2. Install dependencies:

   ```bash
   cd luhn-js-validation &&
   yarn install
   ```

3. Running the Project
   To run the project in development mode:

   ```bash
   yarn start
   ```

### Folder Structure

```/src
  /services
  server.ts
```

### Contributing

You can modify if you want this repo.

### License

This project is licensed under the MIT License.
