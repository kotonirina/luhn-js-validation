import dotenv from "dotenv";
import pino from "pino";

// Load environment variables from .env file
dotenv.config();

export const UIN_GENERATOR_URL = process.env.UIN_GENERATOR_URL ?? "";
export const UIN_GENERATOR_TOKEN = process.env.UIN_GENERATOR_TOKEN ?? "";
export const SMS_PROVIDER = process.env.SMS_PROVIDER ?? "";
export const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID ?? "";
export const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY ?? "";
export const AWS_REGION_NAME = process.env.AWS_REGION_NAME ?? "";
export const AWS_SENDER_ID = process.env.AWS_SENDER_ID ?? "";
export const E_FOKONTANY_UIN_DESTINATOR = "e-fokontany";

export interface IGenerateUinResponse {
  batchId: string;
  createdOn: string;
  theNuis: string[];
  status: string;
}

interface TheUINUpdated {
  nui: string;
  destinator: string;
  batch_label: string;
  created_at: string;
  updated_at: string;
  status: string;
}

export interface IUpdatedUinResponse {
  status: string;
  status_code: number;
  message: string;
  theNuisUpdated: TheUINUpdated[];
}

interface Uin {
  NUI: string;
}

export interface ICustomUinResult {
  theUins: Uin[];
  sheetName: string;
}

export function sleep(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export const logger = pino();
if (process.env.NODE_ENV === "test") {
  logger.level = "silent";
}
