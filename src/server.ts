const bodyParser = require("body-parser");
import express, { Express, Request, Response } from "express";

import {
  E_FOKONTANY_UIN_DESTINATOR,
  ICustomUinResult,
  IGenerateUinResponse,
  IUpdatedUinResponse,
  UIN_GENERATOR_TOKEN,
  UIN_GENERATOR_URL,
  logger,
  sleep,
} from "../constants";
import {
  checkUniqueIdentifierNumber,
  generateRandomUniqueIdentifierNumberParts,
  generateUniqueIdentifierNumber,
  isValidLuhn,
  generateCitizenUniqueId,
  generateXlsxFile,
  sendSMSAwsSns,
  splitXlsxIntoMultipleFiles,
  mergeXLSXFiles,
  extractSheetContentFromXlsxFile,
  updateUinBatchLabel,
} from "./services";
import { findCommonDuplicateValues } from "./services/check-duplicate";
import { splitRowsToSheets } from "./services/split-rows-to-sheets";

const app = express();
interface UinParams {
  uin: string;
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const PORT: string | number = process.env.PORT || 3000;

app.get("/env", (req: Request, res: Response) => {
  res.json({ url: UIN_GENERATOR_URL, toke: UIN_GENERATOR_TOKEN });
});

app.get("/", (req: Request, res: Response) => {
  res.json({ hello: "Luhn API Check Validation" });
});

app.get("/api/v1/randomNumberGenerator", (req: Request, res: Response) => {
  const theNui = generateRandomUniqueIdentifierNumberParts();
  res.json({ theNui: theNui });
});

app.get("/api/v1/generate", (req: Request, res: Response) => {
  const theNui = generateUniqueIdentifierNumber();
  res.json({ theNui: theNui });
});

app.get("/api/v1/checkUin/:uin", (req: Request<UinParams>, res: Response) => {
  const { uin } = req.params;
  const isValid = checkUniqueIdentifierNumber(uin);
  const isValidLuhnAlgo = isValidLuhn(uin);
  res.json({ uin: uin, isValid: isValid, isValidLuhnAlgo: isValidLuhnAlgo });
});

app.get(
  "/api/v1/generateUINWithBatchStartAndLoopSizeAndBatchSizeQueriesParams",
  async (req: Request, res: Response) => {
    const loopSizeParam: number =
      parseInt(req.query.loopSize as string, 10) || 0;
    const batchSizeParam: number =
      parseInt(req.query.batchSize as string, 10) || 0;

    let nuis: ICustomUinResult[] = [];

    let counter = 1;
    let batchCount = parseInt(req.query.batchStart as string, 10) || 1;
    while (counter <= loopSizeParam) {
      logger.info(
        `Processing batch ${counter} of size ${loopSizeParam} => ${batchSizeParam}`
      );
      const result: IGenerateUinResponse | undefined =
        await generateCitizenUniqueId(
          E_FOKONTANY_UIN_DESTINATOR,
          batchSizeParam
        );

      if (result !== undefined) {
        const myUIN = result?.theNuis.map((value) => ({ NUI: value }));
        const sheetName = `batch-${batchCount.toString().padStart(5, "0")}`;
        myUIN !== undefined &&
          nuis.push({ theUins: myUIN, sheetName: sheetName });
      }
      counter++;
      batchCount++;
      await sleep(1000);
    }

    generateXlsxFile(nuis);

    res.json({ status: "finished" });
  }
);

app.get("/api/v1/notifyMe", async (req: Request, res: Response) => {
  const msisdnParam: string = (req.query.msisdn as string) ?? "+261324065687";
  const messageParam: string =
    (req.query.message as string) ?? "Mandefa sms za, voarainao ve andrana ity";
  await sendSMSAwsSns(msisdnParam, messageParam);
  res.json({ msisdn: msisdnParam, message: messageParam });
});

app.get(
  "/api/v1/splitXlsxIntoMultipleFiles",
  async (req: Request, res: Response) => {
    const sheetsPerFile: number =
      parseInt(req.query.sheetsPerFile as string, 10) || 0;
    const startFileIndex: number =
      parseInt(req.query.startFileIndex as string, 10) || 0;
    const filePath: string =
      (req.query.filePath as string) ??
      "data/input/The-efokontany-NUIs-Thu Feb 15 2024.xlsx";

    splitXlsxIntoMultipleFiles(sheetsPerFile, filePath, startFileIndex);
    res.json({ status: "split xlsx file done" });
  }
);

app.get("/api/v1/mergeXLSXFiles", async (req: Request, res: Response) => {
  const filepathsQueryParam =
    (req.query.filepaths as string) ??
    "data/input/container-3-20k.xlsx, data/input/container-4-20k.xlsx";
  const filepaths: string[] = filepathsQueryParam
    ?.split(",")
    .map((filepath) => filepath.trim());

  mergeXLSXFiles(filepaths);
  res.json({ status: "xlsx files combined" });
});

app.get(
  "/api/v1/findCommonDuplicateValues",
  async (req: Request, res: Response) => {
    const filePath: string =
      (req.query.filePath as string) ??
      "data/input/The-efokontany-NUIs-Thu Apr 11 2024-11-18.xlsx";
    const commonDuplicates = findCommonDuplicateValues(filePath);
    res.json({ commonDuplicates: commonDuplicates });
  }
);

app.get("/api/v1/splitRowsToSheets", async (req: Request, res: Response) => {
  const filePath: string =
    (req.query.filePath as string) ??
    "data/input/The-efokontany-NUIs-Fri Apr 12 2024-23-52.xlsx";
  const batchSize: number = parseInt(req.query.batchSize as string, 10) || 2000;
  const sheetNamePrefix: string =
    (req.query.sheetNamePrefix as string) ?? "batch-03179";
  const newWorkbook = splitRowsToSheets(filePath, batchSize, sheetNamePrefix);
  res.json({ newWorkbook: newWorkbook });
});

app.get(
  "/api/v1/extractSheetContentFromXlsxFileAndupdateUinBatchLabel",
  async (req: Request, res: Response) => {
    const filepath =
      (req.query.filepath as string) ?? "data/input/container-3-20k.xlsx";

    const sheetContent = extractSheetContentFromXlsxFile(filepath);
    const entries = Object.entries(sheetContent);
    const updatedUINs: IUpdatedUinResponse[] = [];
    entries.map(async ([key, value]) => {
      const uinsResponse: IUpdatedUinResponse | undefined =
        await updateUinBatchLabel(E_FOKONTANY_UIN_DESTINATOR, key, value);
      updatedUINs.push(uinsResponse as IUpdatedUinResponse);
    });
    res.json({
      message:
        "extract sheet content from xlsx File and update UIN batch label done",
      updatedUINs: updatedUINs,
    });
  }
);

app.listen(PORT, () => {
  console.log(`server is running on ${PORT}`);
});
