import * as XLSX from "xlsx";
import * as fs from "fs";

export function splitRowsToSheets(
  filePath: string,
  batchSize: number,
  sheetNamePrefix: string
): XLSX.WorkBook {
  const fileData = fs.readFileSync(filePath);
  const workbook = XLSX.read(fileData, { type: "buffer" });

  const sheetName = workbook.SheetNames[0];
  const sheet = workbook.Sheets[sheetName];
  const data: any[] = XLSX.utils.sheet_to_json(sheet);

  const numberOfSheets = Math.ceil(data.length / batchSize);

  const newWorkbook: XLSX.WorkBook = XLSX.utils.book_new();

  const rawBatchName = sheetNamePrefix.split("-");

  for (let i = 0; i < numberOfSheets; i++) {
    const startIndex = i * batchSize;
    const endIndex = Math.min((i + 1) * batchSize, data.length);
    const slicedData = data.slice(startIndex, endIndex);

    const sheetName = `${rawBatchName[0]}-${parseInt(rawBatchName[1]) + i}`;
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(slicedData);
    XLSX.utils.book_append_sheet(newWorkbook, worksheet, sheetName);
  }

  XLSX.writeFile(
    newWorkbook,
    `data/input/The-efokontany-NUIs-${new Date().toDateString()}-${new Date()
      .getHours()
      .toString()}-${new Date().getMinutes().toString()}.xlsx`
  );

  return newWorkbook;
}
