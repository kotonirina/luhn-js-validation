import * as XLSX from "xlsx";
import * as fs from "fs";

export const extractSheetContentFromXlsxFile = (
  filePath: string
): Record<string, string[]> => {
  const fileData = fs.readFileSync(filePath);

  const workbook = XLSX.read(fileData, { type: "buffer" });
  const sheetContent: Record<string, string[]> = {};

  workbook.SheetNames.forEach((sheetName) => {
    const sheet = workbook.Sheets[sheetName];
    /**
     *  with header is as value as string with the content
     * 'batch-00184': [
     *                 [ 'NUI' ],
     *                 [ 'XXXXXXXXXXXXXXT' ],
     *                 [ 'XXXXXXXXXXXXXXX' ],
     *                 [ 'XXXXXXXXXXXXXXY' ],
     *                ]
     */
    // const sheetValues = XLSX.utils.sheet_to_json<string[]>(sheet, {
    //   header: 1,
    // });
    /**
     *  with header is as key of string value content
     * 'batch-00181': [{ NUI: 'XXXXXXXXXXXXXXT' }, { NUI: 'XXXXXXXXXXXXXXX' }, { NUI: 'XXXXXXXXXXXXXXY' }, ...]
     */
    // const sheetValues = XLSX.utils.sheet_to_json<string[]>(sheet, {
    //   header: 0,
    // });
    // Or const sheetValues = XLSX.utils.sheet_to_json<string[]>(sheet);

    /**
     *  without header
     * 'batch-00181': ['XXXXXXXXXXXXXXT', 'XXXXXXXXXXXXXXX''XXXXXXXXXXXXXXY', ...]
     */
    const sheetValues = XLSX.utils
      .sheet_to_json<string[]>(sheet)
      .map(Object.values)
      .flatMap((_sheetValue) => _sheetValue);
    sheetContent[sheetName] = sheetValues;
  });

  return sheetContent;
};
