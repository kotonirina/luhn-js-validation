import * as XLSX from "xlsx";
import * as fs from "fs";

export const mergeXLSXFiles = (filepaths: string[]) => {
  const combinedWorkbook: XLSX.WorkBook = { Sheets: {}, SheetNames: [] };
  function mergeXLSX(filepath: string) {
    const fileData = fs.readFileSync(filepath);
    const workbook = XLSX.read(fileData, { type: "buffer" });

    for (const sheetName of workbook.SheetNames) {
      const sheet = workbook.Sheets[sheetName];

      let uniqueSheetName = sheetName;
      let count = 1;
      while (combinedWorkbook.SheetNames.includes(uniqueSheetName)) {
        uniqueSheetName = `${sheetName}_${count}`;
        count++;
      }

      combinedWorkbook.SheetNames.push(uniqueSheetName);
      combinedWorkbook.Sheets[uniqueSheetName] = sheet;
    }
  }

  filepaths.forEach((filepath) => mergeXLSX(filepath));

  XLSX.writeFile(
    combinedWorkbook,
    `data/output/combined-${new Date().toDateString()}-${new Date()
      .getHours()
      .toString()}-${new Date().getMinutes().toString()}.xlsx`,
    {
      bookType: "xlsx",
    }
  );

  console.log("XLSX files combined successfully.");
};
