import * as fs from "fs";
import { parse, Options } from "json2csv";

type Data = Record<string, Record<string, string>[]>;

export function convertJSONToCSV(jsonData: Data): void {
  const csvData = Object.keys(jsonData).map((key: keyof Data) => {
    const options: Options<Record<string, string>> = {};
    return { sheetName: key, csv: parse(jsonData[key], options) };
  });

  const mergedCSV = csvData
    .map((item) => `[${item.sheetName}]\n${item.csv}\n`)
    .join("\n");

  fs.writeFileSync("data/input/merged.csv", mergedCSV);

  console.log("Merged CSV data has been written to merged.csv");
}
