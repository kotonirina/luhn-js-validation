/**
 *  yarn add luhn-js or npm i luhn-js
 * @doc  https://en.wikipedia.org/wiki/Luhn_algorithm.
 * @jean Customize & adapt to our needs
 */

import * as Luhn from "luhn-js";

/**
 * Generate 9 rendom numbers
 *
 * @returns string
 */
export function generateRandomUniqueIdentifierNumberParts(): string {
  let d = new Date().getTime();
  return "xxxxxxxxx".replace(/[xy]/g, (c) => {
    let r = Math.random() * 10;
    r = (d + r) % 10 | 0;
    d = Math.floor(d / 10);
    return (c == "x" ? r : (r & 0x8) | 0x9).toString(10);
  });
}

/**
 * Generate 10 unique identifier number
 * with key control in the last
 *
 * @returns string
 */

export function generateUniqueIdentifierNumber(): string {
  const rawUniqueIdentifierNumber = generateRandomUniqueIdentifierNumberParts();
  return Luhn.generate(rawUniqueIdentifierNumber);
}

/**
 * Check the unique identifier number value
 *
 * @returns boolean
 */
export function checkUniqueIdentifierNumber(theUin: string): boolean {
  return Luhn.isValid(theUin);
}

/**
 * Check the unique identifier number algorithm
 * inspired by the official documenation
 * https://en.wikipedia.org/wiki/Luhn_algorithm.
 *
 * @returns boolean
 */
export function isValidLuhn(input: string): boolean {
  const sanitizedInput = input.replace(/\D/g, ""); // Remove non-digit characters

  let sum = 0;
  let isOdd = false;

  for (let i = sanitizedInput.length - 1; i >= 0; i--) {
    let digit = parseInt(sanitizedInput[i], 10);

    if (isOdd) {
      digit *= 2;
      if (digit > 9) {
        digit -= 9;
      }
    }

    sum += digit;
    isOdd = !isOdd;
  }

  return sum % 10 === 0;
}
