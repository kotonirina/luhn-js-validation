import * as XLSX from "xlsx";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import axios, { AxiosRequestConfig } from "axios";
import {
  UIN_GENERATOR_URL,
  UIN_GENERATOR_TOKEN,
  IGenerateUinResponse,
  ICustomUinResult,
} from "../../constants";
import { generateUniqueIdentifierNumber } from "./validate";

async function generate(batchSize: number) {
  const nuiBatch: string[] = [];

  while (nuiBatch.length < batchSize) {
    const theNui = generateUniqueIdentifierNumber();

    if (!nuiBatch.includes(theNui) && !theNui.startsWith("0")) {
      try {
        nuiBatch.push(theNui);
      } catch (error) {
        continue;
      }
    } else {
      continue;
    }
  }

  return {
    batchId: uuidv4(),
    createdOn: new Date(),
    theNuis: nuiBatch,
    status: "GENERATED",
  } as unknown as IGenerateUinResponse;
}

export async function generateCitizenUniqueId(
  destinator: string,
  batchSize: number
): Promise<IGenerateUinResponse | undefined> {
  try {
    const axiosConfig: AxiosRequestConfig = {
      method: "post",
      url: `${UIN_GENERATOR_URL}/generate`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${UIN_GENERATOR_TOKEN}`,
      },
      data: JSON.stringify({
        batchSize: batchSize,
        destinator: destinator,
      }),
    };
    const response = await axios(axiosConfig);
    return response.data as IGenerateUinResponse | undefined;
  } catch (error) {
    console.log("Error: ", error);
  }
  // return generate(batchSize);
  return undefined;
}

function exportDataToXlsx(
  data: any[],
  workbook: XLSX.WorkBook,
  sheetName: string
) {
  const worksheet = XLSX.utils.json_to_sheet(data);
  XLSX.utils.book_append_sheet(workbook, worksheet, sheetName);
}

export function generateXlsxFile(data: ICustomUinResult[]) {
  const workbook = XLSX.utils.book_new();

  data.forEach((_myData: ICustomUinResult, index: number) => {
    exportDataToXlsx(_myData.theUins, workbook, _myData.sheetName);
  });

  const filePath = `data/input/The-efokontany-NUIs-${new Date().toDateString()}-${new Date()
    .getHours()
    .toString()}-${new Date().getMinutes().toString()}.xlsx`;

  try {
    XLSX.writeFile(workbook, filePath);
  } catch (error) {
    console.error(`Error reading file at path ${filePath}:`, error);
  }
}
