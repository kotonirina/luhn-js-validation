import * as XLSX from "xlsx";
import * as fs from "fs";
// import * as path from "path";

export const splitXlsxIntoMultipleFiles = (
  sheetsPerFile: number = 10,
  filePath: string,
  startFileIndex: number = 0
) => {
  const fileData = fs.readFileSync(filePath);

  const workbook = XLSX.read(fileData, { type: "buffer" });

  const numOutputFiles = Math.ceil(workbook.SheetNames.length / sheetsPerFile);
  for (let fileIndex = 0; fileIndex < numOutputFiles; fileIndex++) {
    const newWorkbook: XLSX.WorkBook = { Sheets: {}, SheetNames: [] };

    for (let i = 0; i < sheetsPerFile; i++) {
      const sheetIndex = fileIndex * sheetsPerFile + i;
      if (sheetIndex >= workbook.SheetNames.length) break;

      const sheetName = workbook.SheetNames[sheetIndex];
      const sheet = workbook.Sheets[sheetName];
      newWorkbook.SheetNames.push(sheetName);
      newWorkbook.Sheets[sheetName] = sheet;
    }

    const outputFileName = `data/output/container-${
      startFileIndex + (fileIndex + 1)
    }-20k.xlsx`;

    XLSX.writeFile(newWorkbook, outputFileName, { bookType: "xlsx" });

    console.log(
      `File ${outputFileName} created with ${newWorkbook.SheetNames.length} sheets.`
    );
  }

  console.log("Extraction completed!");
};
