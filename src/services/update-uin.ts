import * as XLSX from "xlsx";
import fs from "fs";
import axios, { AxiosRequestConfig } from "axios";
import {
  UIN_GENERATOR_URL,
  UIN_GENERATOR_TOKEN,
  IUpdatedUinResponse,
} from "../../constants";

export async function updateUinBatchLabel(
  destinator: string,
  uinBatchLabel: string,
  theUINs: string[]
): Promise<IUpdatedUinResponse | undefined> {
  try {
    const axiosConfig: AxiosRequestConfig = {
      method: "put",
      url: `${UIN_GENERATOR_URL}/batch/update`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${UIN_GENERATOR_TOKEN}`,
      },
      data: JSON.stringify({
        nui: theUINs,
        destinator: destinator,
        batch_label: uinBatchLabel,
      }),
    };
    const response = await axios(axiosConfig);
    return response.data as IUpdatedUinResponse | undefined;
  } catch (error) {
    console.log("Error: ", error);
  }
  return undefined;
}
