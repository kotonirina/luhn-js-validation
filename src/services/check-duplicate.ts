import * as XLSX from "xlsx";
import * as fs from "fs";

interface DuplicateValue {
  value: string;
  sheets: string[];
}

export function findCommonDuplicateValues(filePath: string): DuplicateValue[] {
  const fileData = fs.readFileSync(filePath);
  const workbook = XLSX.read(fileData, { type: "buffer" });

  const sheetValuesMap: Map<string, Set<string>> = new Map();

  workbook.SheetNames.forEach((sheetName) => {
    const sheet = workbook.Sheets[sheetName];

    const uniqueValues = new Set<string>();

    XLSX.utils.sheet_to_json(sheet).forEach((row: any) => {
      Object.values(row).forEach((cellValue: any) => {
        uniqueValues.add(cellValue.toString());
      });
    });

    sheetValuesMap.set(sheetName, uniqueValues);
  });

  const commonDuplicates: DuplicateValue[] = [];

  const firstSheetValues = sheetValuesMap.get(workbook.SheetNames[0])!;
  firstSheetValues.forEach((value) => {
    const existsInAllSheets = workbook.SheetNames.slice(1).every(
      (sheetName) => {
        const valuesSet = sheetValuesMap.get(sheetName)!;
        return valuesSet.has(value);
      }
    );

    if (existsInAllSheets) {
      const sheets = [workbook.SheetNames[0], ...workbook.SheetNames.slice(1)];
      commonDuplicates.push({
        value: value,
        sheets: sheets,
      });
    }
  });

  return commonDuplicates;
}
